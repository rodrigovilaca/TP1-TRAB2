import tkinter as tk                
from tkinter import font  as tkfont 
from tkinter.ttk import *

import numpy as np
import cv2
import os
from pessoa import Pessoa
from reserva import Reserva
from funcs import *
import pickle
import builtins
import random
import datetime

builtins.LOGIN = False
builtins.USER = None
#builtins.LOGIN = True



class LINF(tk.Tk): #Frame principal do programa, que chama o frame que será renderizado, e contém algumas definições globais
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        
        self.style = Style()
        self.style.theme_use('clam')

        self.title_font = tkfont.Font(family='Helvetica', size=14, weight="bold", slant="italic")
        self.message_font = tkfont.Font(family="Helvetica", size=10)
        self.labelFont = tkfont.Font(family="Helvetica", size=10, weight="bold")

        self.edit_reserva = 0

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, Login, Cadastro, Reservas, MenuReserva, EscolherReserva.EditarReservas, EscolherReserva):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage") #renderiza, na construção, a página StartPage

    def show_frame(self, page_name):
        if page_name == "StartPage":
            frame = self.frames[page_name]
            frame.updateMain()
            frame.tkraise()
        elif page_name == "Reservas":
            frame = self.frames[page_name]
            frame.atualizaReserva()
            frame.tkraise()
        elif page_name == "Cadastro":
            frame = self.frames[page_name]
            frame.atualizaUsuario()
            frame.tkraise()

        elif page_name == "Login":
            frame = self.frames[page_name]
            frame.atualizaLogin()
            frame.tkraise()
        elif page_name == "EscolherReserva":
            frame = self.frames[page_name]
            frame.atualizaReservas()
            frame.tkraise()
        elif page_name == "EditarReservas":
            frame = self.frames[page_name]
            frame.refresh(self)
            frame.tkraise()

        frame = self.frames[page_name]
        frame.tkraise()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.label = tk.Label(self, text="Bem vindo ao LINF!", font=controller.title_font)
        self.label.pack(side="top", fill="x", pady=10)

        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer.pack(fill="x")

        self.quartoContainer = tk.Frame(self, parent)
        self.quartoContainer["padx"] = 20
        self.quartoContainer.pack(fill="x")
        
        

        if LOGIN:
            self.usuario = tk.Label(self.primeiroContainer, text="Usuário: %s %s"%(USER.nome, USER.sobrenome), fg="blue", anchor="w", state="disabled")
            self.usuario.pack(side="top", fill="x", pady=2)
        

        else:
            self.usuario = tk.Label(self.primeiroContainer, text="", fg="blue", anchor="w", state="disabled")
            self.usuario.pack(side="top", fill="x", pady=2)
            self.reserva = tk.Message(self.primeiroContainer,aspect=500, text="", fg="red", anchor="w")

        self.contReservas=[]
        
        self.button1 = Button(self.segundoContainer, text="Login",
                            command=lambda: controller.show_frame("Login"))
        self.button2 = Button(self.terceiroContainer, text="Cadastro",
                            command=lambda: controller.show_frame("Cadastro"))
        if (LOGIN and USER.prof == True) or (LOGIN and USER.servidor == True):
            self.button3 = Button(self.quartoContainer, text="Reservas",
                            command=lambda: controller.show_frame("MenuReserva"), state="normal")
        else:
            self.button3 = Button(self.quartoContainer, text="Reservas",
                            command=lambda: controller.show_frame("MenuReserva"), state = "disabled")
        self.button4 = Button(self.segundoContainer, text="Logout",
                            command=lambda:self.Logout())
        
        self.button1.pack(fill="x", pady="5")
        self.button2.pack(fill="x", pady="5")
        self.button3.pack(fill="x", pady="5")

    def updateMain(self):
        print(LOGIN)
        if LOGIN: 
            self.usuario["text"]="Usuário: %s %s"%(USER.nome, USER.sobrenome)

            if (USER.prof == True) or (USER.servidor == True):
                self.button3["state"]="normal"
                self.button2["state"]='normal'
            else:
                self.button2["state"]='disabled'
                
            try:
                for i in self.contReservas:
                    i.pack_forget()
            except:
                self.reserva["text"]="" 

            self.contReservas=[]
            for iden in USER.ident:
                reser = RetornaReserva(iden)
                try: horario = " e ".join(reser.horario)
                except: horario = reser.horario[0]
                try: recorrencia = "/".join(reser.recorrencia)
                except: recorrencia = reser.recorrencia[0]
                reservas = "Reserva: %s\n"%(reser)
                self.reserva = tk.Message(self.primeiroContainer,aspect=800, text=reservas, fg="red", anchor="w")
                semana={
                    "seg":0, "ter":1, "qua":2, "qui":3, "sex":4, "sab":5
                }
                for hor in reser.horario: #verifica a autorização de entrada
                    hour = datetime.datetime.now().hour
                    minutes = datetime.datetime.now().minute
                    dia = datetime.date.today()
                    dia = dia.weekday()
                    for recorr in reser.recorrencia:
                        if semana[recorr]==dia:
                            test = hor.split(':')
                            #print(hour, minutes, test)
                            self.reserva["fg"]="red"
                            if (hour>=int(test[0]) and hour<=int(test[0])+2):
                                print("teste1")
                                if (hour==int(test[0])) and minutes>=int(test[1]):
                                    self.reserva["fg"]="green"
                                    print("teste")
                                else:
                                    self.reserva["fg"]="green"
                        self.reserva.pack(side="bottom", fill="x", pady=0)
                        self.contReservas.append(self.reserva)
            
            self.usuario["state"]="normal"

            self.button1.pack_forget()  
            
            self.button4.pack(fill="x", pady="5")
        else:
            self.button3["state"]="disabled"
            self.button2["state"]="normal"
            self.usuario["text"]=""
            #self.reserva["text"]=""
            try:
                for i in self.contReservas:
                    i.pack_forget()
            except:
                self.reserva["text"]=""
            self.usuario["state"]="disabled"
            self.button4.pack_forget()

            self.button1.pack(fill="x", pady="5")

    def Logout(self):
        global LOGIN
        global USER
        LOGIN = False
        USER = None
        self.updateMain()

#-------------------------------------------------------------------------------------#

class MenuReserva(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.label = tk.Label(self, text="Reservas", font=controller.title_font)
        self.label.pack(side="top", fill="x", pady=10)

        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer.pack(fill="x")       
        
        self.button1 = Button(self.primeiroContainer, text="Editar Reservas",
                            command=lambda: controller.show_frame("EscolherReserva"), state='disabled')
        self.button2 = Button(self.segundoContainer, text="Cadastrar Reservas",
                            command=lambda: controller.show_frame("Reservas"))
        self.button3 = Button(self.terceiroContainer, text="Voltar para o início",
                           command=lambda: controller.show_frame("StartPage"))
        self.button3.pack(fill="x")
        
        self.button1.pack(fill="x", pady="5")
        self.button2.pack(fill="x", pady="5")



#-------------------------------------------------------------------------------------#
class Login(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Login", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["pady"] = 10
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer.pack(fill="x")

        self.nomeLabel = tk.Label(self.segundoContainer,text="Matricula/CPF", font=controller.labelFont, width="15", anchor="w")
        self.nomeLabel.pack(side="left")

        self.numero = Entry(self.segundoContainer)
        self.numero.pack(fill="x")

        self.autenticar = Button(self.terceiroContainer)
        self.autenticar["text"] = "Logar"

        self.autenticar["command"] = self.logar
        self.autenticar.pack(fill="x")

        button = Button(self.terceiroContainer, text="Voltar para o início",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack(fill="x")
        self.mensagem = tk.Label(self.primeiroContainer, text="", font=controller.message_font, fg="red")
        self.mensagem.pack(fill="x", side="top")

    #Método verificar login
    def logar(self):
        numero = self.numero.get()
        global LOGIN
        global USER
        if not (BuscaObjeto(numero)):
            self.mensagem["text"] = "Matricula invalida"
            return
        LOGIN, USER = Logar(numero)
        if LOGIN:
            
            LOGIN=True
            self.mensagem["fg"] = "green"
            self.mensagem["text"] = "Login realizado com sucesso"

    def atualizaLogin(self):
        self.mensagem["text"]=""
        self.mensagem["fg"]="red"

class Cadastro(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Cadastro", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
    # -------------------------------------------------------------------------#
        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer["pady"] = 3
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer["pady"] = 3
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer["pady"] = 3
        self.terceiroContainer.pack(fill="x")

        self.quartoContainer = tk.Frame(self, parent)
        self.quartoContainer["padx"] = 20
        self.quartoContainer["pady"] = 3
        self.quartoContainer.pack(fill="x")

        self.quintoContainer = tk.Frame(self, parent)
        self.quintoContainer["padx"] = 20
        self.quintoContainer["pady"] = 3
        self.quintoContainer.pack(fill="x")

        self.sextoContainer = tk.Frame(self, parent)
        self.sextoContainer["pady"] = 20
        self.sextoContainer["pady"] = 3
        self.sextoContainer.pack(fill="x")

        self.setimoContainer = tk.Frame(self, parent)
        self.setimoContainer["pady"] = 20
        self.setimoContainer["pady"] = 3
        self.setimoContainer.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.mensagem = tk.Label(self.primeiroContainer, text="", font=controller.message_font, fg="red")
        self.mensagem.pack(side="top")

        self.nomeLabel = tk.Label(self.primeiroContainer,text="Nome", font=controller.labelFont, width="15", anchor="w")
        self.nomeLabel.pack(side="left")

        self.nome = Entry(self.primeiroContainer)
        self.nome["font"] = controller.labelFont
        self.nome.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.sobrenomeLabel = tk.Label(self.segundoContainer, text="Sobrenome", font=controller.labelFont, width="15", anchor="w")
        self.sobrenomeLabel.pack(side="left")

        self.sobrenome = Entry(self.segundoContainer)
        self.sobrenome["font"] = controller.labelFont
        self.sobrenome.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.numeroLabel = tk.Label(self.terceiroContainer, text="Matricula/CPF", font=controller.labelFont, width="15", anchor="w")
        self.numeroLabel.pack(side="left")

        self.numero = Entry(self.terceiroContainer)
        self.numero["font"] = controller.labelFont
        self.numero.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.roleLabel = tk.Label(self.quartoContainer, text="Tipo de Usuario", font=controller.labelFont, width="15", anchor="w")
        self.roleLabel.pack(side="left")

        self.role = tk.IntVar(self.quartoContainer)
        tk.Radiobutton(self.quartoContainer, text="Aluno", variable=self.role, value=0).pack(anchor="w")
        tk.Radiobutton(self.quartoContainer, text="Professor", variable=self.role, value=1).pack(anchor="w")
        tk.Radiobutton(self.quartoContainer, text="Servidor", variable=self.role, value=2).pack(anchor="w")
    # -------------------------------------------------------------------------#
        self.reservaLabel = tk.Label(self.quintoContainer, text="Reservas", font=controller.labelFont, width="15", anchor="w")
        self.reservaLabel.pack(side="left")

        self.vsb = tk.Scrollbar(self.quintoContainer, orient="vertical")
        self.text = tk.Text(self.quintoContainer, width=40, height=10, 
                            yscrollcommand=self.vsb.set, background="lightgrey", state='disabled')
        self.vsb.config(command=self.text.yview)
        
    # -------------------------------------------------------------------------#
        self.autenticar = Button(self.sextoContainer)
        self.autenticar["text"] = "Cadastrar Rosto"

        self.autenticar["command"] = self.cadastrar
        self.autenticar.pack(fill="x", pady="3")

        button = Button(self.sextoContainer, text="Voltar para o início",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack(fill="x", pady="3")        


    def atualizaUsuario(self):
        self.vsb.pack(side="right", fill="both")
        self.text.pack(side="left", fill="both", expand=True)
        self.text['state']='normal'
        self.text.delete(1.0, "end")
        self.text['state']='disabled'
        self.reservas=[]
        for i in LISTARES:
            var =tk.IntVar() 
            #cb = tk.Checkbutton(self.quartoContainer, text="%s | %s | %s %s" % (i.codigoDis, i.prop, recorrencia, horario), variable=var)
            cb = tk.Checkbutton(self.quintoContainer, text="%s" % (i), variable=var)
            self.text.window_create("end", window=cb)
            self.text.insert("end", "\n") # to force one checkbox per line
            self.reservas.append(var)

        self.mensagem["text"]=""
        self.mensagem["fg"]="red"
        self.autenticar["state"] = "normal"

    def cadastrar(self):
        nome = self.nome.get()
        if len(nome) < 1: 
            self.mensagem["text"]="Nome Inválido!"
            return
        sobrenome = self.sobrenome.get()
        if len(sobrenome) < 1: 
            self.mensagem["text"]="Sobrenome Inválido!"
            return
        numero = self.numero.get()
        if len(numero) < 1:
            self.mensagem["text"]="Matrícula inválida!"
            return
        try: int(numero)
        except:
            self.mensagem["text"]="Matrícula inválida!"
            return
        role = self.role.get()
        if (role == 1):
            prof = True
            serv = False
        elif (role == 2):
            serv = True
            prof = False
        elif (role == 0):
            serv = False
            prof = False

        obj = Pessoa(nome, sobrenome, numero, prof, serv)
        reservas=[]
        for res in range(len(LISTARES)):
            if self.reservas[res].get() != 0:
                reservas.append(LISTARES[res].codigoDis)
        obj.ident = reservas
        if len(reservas) > 0:
            for reserva in reservas:
                obj2 = RetornaReserva(reserva)
                obj2.participantes.append(numero)
        obj.tiraFoto()
        LISTAOBJS.append(obj)
        self.mensagem["fg"] = "green"
        self.mensagem["text"] = "Face cadastrada com sucesso!\nUsuário %s cadastrado com sucesso"%(obj.nome)
        self.autenticar["state"] = "disabled"
#----------------------------------------------------------------------------------#            

class Reservas(tk.Frame):

    def __init__(self, parent, controller, frame_look={}, **look):
        args = dict(relief=tk.SUNKEN, border=1)
        args.update(frame_look)
        tk.Frame.__init__(self, parent)
        
        
        self.controller = controller
        label = tk.Label(self, text="Cadastrar Reserva", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
    # -------------------------------------------------------------------------#
        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer["pady"] = 3
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer["pady"] = 3
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer["pady"] = 3
        self.terceiroContainer.pack(fill="x")

        self.quartoContainer = tk.Frame(self, parent)
        self.quartoContainer["padx"] = 20
        self.quartoContainer["pady"] = 3
        self.quartoContainer.pack(fill="x")

        self.quintoContainer = tk.Frame(self, parent)
        self.quintoContainer["padx"] = 20
        self.quintoContainer["pady"] = 3
        self.quintoContainer.pack(fill="x")

        self.sextoContainer = tk.Frame(self, parent)
        self.sextoContainer["padx"] = 20
        self.sextoContainer["pady"] = 3
        self.sextoContainer.pack(fill="x")

        self.setimoContainer = tk.Frame(self, parent)
        self.setimoContainer["padx"] = 20
        self.setimoContainer["pady"] = 3
        self.setimoContainer.pack(fill="x")

        self.oitavoContainer = tk.Frame(self, parent)
        self.oitavoContainer["padx"] = 20
        self.oitavoContainer["pady"] = 3
        self.oitavoContainer.pack(fill="x")

        self.nonoContainer = tk.Frame(self, parent)
        self.nonoContainer["padx"] = 20
        self.nonoContainer["pady"] = 3
        self.nonoContainer.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.mensagem = tk.Label(self.primeiroContainer, text="", font=controller.message_font, fg="red")
        self.mensagem.pack(side="top")

        self.tituloLabel = tk.Label(self.primeiroContainer,text="Titulo*", font=controller.labelFont, width="15", anchor="w")
        self.tituloLabel.pack(side="left")

        self.titulo = Entry(self.primeiroContainer)
        self.titulo["font"] = controller.labelFont
        self.titulo.pack(fill="x")
    # -------------------------------------------------------------------------#
        self.turmaLabel = tk.Label(self.terceiroContainer, text="Turma", font=controller.labelFont, width="15", anchor="w")
        self.turmaLabel.pack(side="left")

        self.turma = Entry(self.terceiroContainer, width=4)
        self.turma["font"] = controller.labelFont
        self.turma.pack(anchor='w')
    # -------------------------------------------------------------------------#
        self.salasLabel = tk.Label(self.quartoContainer, text="Salas*", font=controller.labelFont, width="15", anchor="w")
        self.salasLabel.pack(side="left")
        picks=["1", "2", "3", "4"]
        self.salas = []
        for pick in picks:
            var = tk.IntVar()
            chk = tk.Checkbutton(self.quartoContainer, text=pick, variable=var)
            chk.pack(side="left", anchor="w", expand="yes")
            self.salas.append(var)
    # -------------------------------------------------------------------------#
        self.horarioLabel = tk.Label(self.quintoContainer, text="Horário*", font=controller.labelFont, width="15", anchor="w")
        self.horarioLabel.pack(side="left")
        picks=["8:00", "10:00", "14:00", "16:00", "19:00", "20:40"]
        self.horario = []
        for pick in picks:
            var = tk.IntVar()
            chk = tk.Checkbutton(self.quintoContainer, text=pick, variable=var)
            chk.pack(side="left", anchor="w", expand="yes")
            self.horario.append(var)
    # -------------------------------------------------------------------------#
        self.recorrLabel = tk.Label(self.sextoContainer, text="Recorrência", font=controller.labelFont, width="15", anchor="w")
        self.recorrLabel.pack(side="left")
        picks=["seg", "ter", "qua", "qui", "sex", "sab"]
        self.recorr = []
        for pick in picks:
            var = tk.IntVar()
            chk = tk.Checkbutton(self.sextoContainer, text=pick, variable=var)
            chk.pack(side="left", anchor="w", expand="yes")
            self.recorr.append(var)
    # -------------------------------------------------------------------------#
        self.fimLabel = tk.Label(self.setimoContainer, text="Fim da Reserva", font=controller.labelFont, width="15", anchor="w")
        self.fimLabel.pack(side="left")

        self.fim_1 = Entry(self.setimoContainer, width=2)
        self.fimLabel_1 = tk.Label(self.setimoContainer, text='/')
        self.fim_2 = Entry(self.setimoContainer, width=2)
        self.fimLabel_2 = tk.Label(self.setimoContainer, text='/')
        self.fim_3 = Entry(self.setimoContainer, width=4)

        self.fim_1.pack(side=tk.LEFT)
        self.fimLabel_1.pack(side=tk.LEFT)
        self.fim_2.pack(side=tk.LEFT)
        self.fimLabel_2.pack(side=tk.LEFT)
        self.fim_3.pack(side=tk.LEFT)

        self.fim_1.bind('<KeyRelease>', self._e1_check)
        self.fim_2.bind('<KeyRelease>', self._e2_check)
        self.fim_3.bind('<KeyRelease>', self._e3_check)
    # -------------------------------------------------------------------------#
        self.reservaLabel = tk.Label(self.oitavoContainer, text="Participantes", font=controller.labelFont, width="15", anchor="w")
        self.reservaLabel.pack(side="left")

        self.vsb = tk.Scrollbar(self.oitavoContainer, orient="vertical")
        self.text = tk.Text(self.oitavoContainer, width=40, height=10, 
                            yscrollcommand=self.vsb.set, background="lightgrey", state='disabled')
        self.vsb.config(command=self.text.yview)
    
            
        self.autenticar = Button(self.nonoContainer)
        self.autenticar["text"] = "Cadastrar Reserva"

        self.autenticar["command"] = self.cadastrarReserva
        self.autenticar.pack(fill="x", pady="3")

        button = Button(self.nonoContainer, text="Voltar para o início",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack(fill="x", pady="3")
    
    def atualizaReserva(self):
        #self.vsb.pack_forget()
        #self.text.pack_forget()
        self.vsb.pack(side="right", fill="both")
        self.text.pack(side="left", fill="both", expand=True)
        self.text['state']='normal'
        self.text.delete(1.0, "end")
        self.text['state']='disabled'
        self.participantes=[]
        self.participantes.clear()
        self.autenticar["state"]='normal'

        for i in LISTAOBJS:
            var =tk.IntVar() 
            cb = tk.Checkbutton(self.oitavoContainer, text="%s | %s %s" % (i.numero, i.nome, i.sobrenome), variable=var)
            self.text.window_create("end", window=cb)
            self.text.insert("end", "\n") # to force one checkbox per line
            self.participantes.append(var)

        self.mensagem["text"]=""
        self.mensagem["fg"]="red"

    
    def _backspace(self, entry):
        cont = entry.get()
        entry.delete(0, tk.END)
        entry.insert(0, cont[:-1])

    def _e1_check(self, e):

        cont = self.fim_1.get()
        if len(cont) >= 2:
            self.fim_2.focus()
        if len(cont) > 2 :
            self._backspace(self.fim_1)
            self.fim_1.focus()

    def _e2_check(self, e):
        cont = self.fim_2.get()
        if len(cont) >= 2:
            self.fim_3.focus()
        if len(cont) > 2 :
            self._backspace(self.fim_2)
            self.fim_2.focus()       

    def _e3_check(self, e):
        cont = self.fim_2.get()
        if len(cont) > 4 :
            self._backspace(self.fim_3)
   

    #Método verificar senha
    def cadastrarReserva(self):
        autor = USER.numero
        prop = self.titulo.get()
        if len(prop) < 1: 
            self.mensagem["text"]="Título Inválido!"
            return
        codigoDis = random.SystemRandom().randint(100000, 999999)
        turma = self.turma.get()
        if len(turma) < 1 or len(turma) >2: 
            self.mensagem["text"]="Turma Inválida!"
            return
        try: 
            int(turma)
            self.mensagem["text"]="Turma não pode ser numérica!"
            return
        except: pass

        numero=[]
        for sala in range( len(self.salas)):
            if self.salas[sala].get() != 0:
                numero.append(sala+1)
        if len(numero) < 1:
            self.mensagem["text"]="É necessário selecionar uma sala!"
            return

        horarios=["8:00", "10:00", "14:00", "16:00", "19:00", "20:40"]
        horario=[]
        for hor in range(len(horarios)):
            if self.horario[hor].get() != 0:
                horario.append(horarios[hor])
        if len(horario) < 1:
            self.mensagem["text"]="É necessário selecionar um Horário!"
            return

        recorrencias=["seg", "ter", "qua", "qui", "sex", "sab"]
        recorrencia=[]
        for rec in range(len(recorrencias)):
            if self.recorr[rec].get() != 0:
                recorrencia.append(recorrencias[rec])
        if len(recorrencia) < 1:
            self.mensagem["text"]="É necessário selecionar a recorrência!"
            return

        fim=[]
        fim.append(self.fim_1.get())
        fim.append(self.fim_2.get())
        fim.append(self.fim_3.get())

        fim = '/'.join(fim)
        participantes=[]
        for part in range(len(LISTAOBJS)):
            try:
                if self.participantes[part].get() != 0:
                    participantes.append(LISTAOBJS[part].numero)
                    print(participantes)

            except: pass

        obj = Reserva(autor, prop, codigoDis, numero, horario)
        if len(turma)>0: obj.turma = turma
        if len(recorrencia)>0: obj.recorrencia = recorrencia
        if len(fim)>5: 
            fim = datetime.datetime.strptime(fim, "%d/%m/%Y")
            obj.fim=fim
            print(obj.fim)
        if len(participantes)>0: 
            obj.participantes = participantes
            for participante in participantes:
                usr = RetornaObjeto(participante)
                usr.ident.append(codigoDis)

        print(obj, obj.turma, obj.recorrencia, obj.fim, obj.participantes)
        self.mensagem["fg"]="green"
        self.mensagem["text"]="Reserva numero %s Cadastrada com Sucesso!"%obj.codigoDis
        LISTARES.append(obj)
        self.autenticar["state"]='disabled'


class EscolherReserva(tk.Frame):
    def __init__(self, parent, controller, frame_look={}, **look):
        args = dict(relief=tk.SUNKEN, border=1)
        args.update(frame_look)
        tk.Frame.__init__(self, parent)
        
        self.controller = controller
        label = tk.Label(self, text="Editar Reserva", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
    # -------------------------------------------------------------------------#
        self.primeiroContainer = tk.Frame(self, parent)
        self.primeiroContainer["padx"] = 20
        self.primeiroContainer["pady"] = 3
        self.primeiroContainer.pack(fill="x")

        self.segundoContainer = tk.Frame(self, parent)
        self.segundoContainer["padx"] = 20
        self.segundoContainer["pady"] = 3
        self.segundoContainer.pack(fill="x")

        self.terceiroContainer = tk.Frame(self, parent)
        self.terceiroContainer["padx"] = 20
        self.terceiroContainer["pady"] = 3
        self.terceiroContainer.pack(fill="x")


    # -------------------------------------------------------------------------#
        self.reservaLabel = tk.Label(self.primeiroContainer, text="Reservas", font=controller.labelFont, width="15", anchor="w")
        self.reservaLabel.pack(side="left")

        self.vsb = tk.Scrollbar(self.primeiroContainer, orient="vertical")
        self.text = tk.Text(self.primeiroContainer, width=40, height=10, 
                            yscrollcommand=self.vsb.set, background="lightgrey", state='disabled')
        self.vsb.config(command=self.text.yview)

        button = Button(self.segundoContainer, text="Editar",
                           command=lambda: self.selecionaReserva(controller))
        button.pack(fill="x", pady="3")    

        button = Button(self.terceiroContainer, text="Voltar para o início",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack(fill="x", pady="3")    

    def selecionaReserva(self, controller):
        controller.edit_reserva = self.codigo.get()
        #print(codigo_reserva)
        controller.show_frame("EditarReservas")


    def atualizaReservas(self):
        self.vsb.pack(side="right", fill="both")
        self.text.pack(side="left", fill="both", expand=True)
        self.text['state']='normal'
        self.text.delete(1.0, "end")
        self.text['state']='disabled'

        self.codigo =tk.IntVar(self.primeiroContainer)
        for i in LISTARES:
            cb = tk.Radiobutton(self.primeiroContainer, text="%s" % (i), variable=self.codigo, value=i.codigoDis)
            self.text.window_create("end", window=cb)
            self.text.insert("end", "\n") # to force one checkbox per line

    class EditarReservas(tk.Frame):

        def __init__(self, parent, controller, frame_look={}, **look):
            args = dict(relief=tk.SUNKEN, border=1)
            args.update(frame_look)
            tk.Frame.__init__(self, parent)
            self.controller = controller
            self.label = tk.Label(self, text="Editar Reserva %s"%str(controller.edit_reserva), font=controller.title_font)
            self.label.pack(side="top", fill="x", pady=10)

            self.primeiroContainer = tk.Frame(self, parent)
            self.primeiroContainer["padx"] = 20
            self.primeiroContainer["pady"] = 3
            self.primeiroContainer.pack(fill="x")

            self.segundoContainer = tk.Frame(self, parent)
            self.segundoContainer["padx"] = 20
            self.segundoContainer["pady"] = 3
            self.segundoContainer.pack(fill="x")

            self.terceiroContainer = tk.Frame(self, parent)
            self.terceiroContainer["padx"] = 20
            self.terceiroContainer["pady"] = 3
            self.terceiroContainer.pack(fill="x")

            self.quartoContainer = tk.Frame(self, parent)
            self.quartoContainer["padx"] = 20
            self.quartoContainer["pady"] = 3
            self.quartoContainer.pack(fill="x")

            self.quintoContainer = tk.Frame(self, parent)
            self.quintoContainer["padx"] = 20
            self.quintoContainer["pady"] = 3
            self.quintoContainer.pack(fill="x")

            self.sextoContainer = tk.Frame(self, parent)
            self.sextoContainer["padx"] = 20
            self.sextoContainer["pady"] = 3
            self.sextoContainer.pack(fill="x")

            self.setimoContainer = tk.Frame(self, parent)
            self.setimoContainer["padx"] = 20
            self.setimoContainer["pady"] = 3
            self.setimoContainer.pack(fill="x")

            self.oitavoContainer = tk.Frame(self, parent)
            self.oitavoContainer["padx"] = 20
            self.oitavoContainer["pady"] = 3
            self.oitavoContainer.pack(fill="x")

            self.nonoContainer = tk.Frame(self, parent)
            self.nonoContainer["padx"] = 20
            self.nonoContainer["pady"] = 3
            self.nonoContainer.pack(fill="x")

        # -------------------------------------------------------------------------#

            codigo_reserva = RetornaReserva(int(controller.edit_reserva))
            if codigo_reserva != None:
                prop = codigo_reserva.prop
                turma = codigo_reserva.turma
                numero = codigo_reserva.numero
                horario = codigo_reserva.horario
                recorrencia = codigo_reserva.recorrencia
                fim = codigo_reserva.fim
                participantes = codigo_reserva.participantes
            else:
                prop = '';turma = '';numero=[]; horario = []; recorrencia = [];fim = '';participantes=[];
        # -------------------------------------------------------------------------#
            self.mensagem = tk.Label(self.primeiroContainer, text="", font=controller.message_font, fg="red")
            self.mensagem.pack(side="top")

            self.tituloLabel = tk.Label(self.primeiroContainer,text="Titulo", font=controller.labelFont, width="15", anchor="w")
            self.tituloLabel.pack(side="left")

            self.titulo = Entry(self.primeiroContainer, text=prop)
            self.titulo["font"] = controller.labelFont
            self.titulo.pack(fill="x")
        # -------------------------------------------------------------------------#
            self.turmaLabel = tk.Label(self.terceiroContainer, text="Turma", font=controller.labelFont, width="15", anchor="w")
            self.turmaLabel.pack(side="left")

            self.turma = Entry(self.terceiroContainer, width=4)
            self.turma["font"] = controller.labelFont
            self.turma.pack(fill="x")
        # -------------------------------------------------------------------------#
            self.salasLabel = tk.Label(self.quartoContainer, text="Salas*", font=controller.labelFont, width="15", anchor="w")
            self.salasLabel.pack(side="left")
            picks=["1", "2", "3", "4"]
            self.salas = []
            for pick in picks:
                var = tk.IntVar()
                chk = tk.Checkbutton(self.quartoContainer, text=pick, variable=var)
                chk.pack(side="left", anchor="w", expand="yes")
                self.salas.append(var)
        # -------------------------------------------------------------------------#
            self.horarioLabel = tk.Label(self.quintoContainer, text="Horário*", font=controller.labelFont, width="15", anchor="w")
            self.horarioLabel.pack(side="left")
            picks=["8:00", "10:00", "14:00", "16:00", "19:00", "20:40"]
            self.horario = []
            for pick in picks:
                var = tk.IntVar()
                chk = tk.Checkbutton(self.quintoContainer, text=pick, variable=var)
                chk.pack(side="left", anchor="w", expand="yes")
                self.horario.append(var)
        # -------------------------------------------------------------------------#
            self.recorrLabel = tk.Label(self.sextoContainer, text="Recorrência", font=controller.labelFont, width="15", anchor="w")
            self.recorrLabel.pack(side="left")
            picks=["seg", "ter", "qua", "qui", "sex", "sab"]
            self.recorr = []
            for pick in picks:
                var = tk.IntVar()
                chk = tk.Checkbutton(self.sextoContainer, text=pick, variable=var)
                chk.pack(side="left", anchor="w", expand="yes")
                self.recorr.append(var)
        # -------------------------------------------------------------------------#
            self.fimLabel = tk.Label(self.setimoContainer, text="Fim da Reserva", font=controller.labelFont, width="15", anchor="w")
            self.fimLabel.pack(side="left")

            self.fim_1 = Entry(self.setimoContainer, width=20)
            

            self.fim_1.pack(side=tk.LEFT)
            

        # -------------------------------------------------------------------------#
            self.reservaLabel = tk.Label(self.oitavoContainer, text="Participantes", font=controller.labelFont, width="15", anchor="w")
            self.reservaLabel.pack(side="left")

            self.vsb = tk.Scrollbar(self.oitavoContainer, orient="vertical")
            self.text = tk.Text(self.oitavoContainer, width=40, height=10, 
                                yscrollcommand=self.vsb.set, background="lightgrey", state='disabled')
            self.vsb.config(command=self.text.yview)
        
                
            self.autenticar = Button(self.nonoContainer)
            self.autenticar["text"] = "Cadastrar Reserva"

            self.autenticar["command"] = self.cadastrarReserva
            self.autenticar.pack(fill="x", pady="3")

            button = Button(self.nonoContainer, text="Voltar para o início",
                               command=lambda: controller.show_frame("StartPage"))
            button.pack(fill="x", pady="3")

        def refresh(self, controller):
            self.controller = controller
            self.label['text'] ="Editar Reserva %s"%str(controller.edit_reserva)

            codigo_reserva = RetornaReserva(int(controller.edit_reserva))
            if codigo_reserva != None:
                prop = codigo_reserva.prop
                turma = codigo_reserva.turma
                numero = codigo_reserva.numero
                horario = codigo_reserva.horario
                recorrencia = codigo_reserva.recorrencia
                fim = codigo_reserva.fim
                participantes = codigo_reserva.participantes
            else:
                prop = '';turma = '';numero=[]; horario = []; recorrencia = [];fim = '';participantes=[];

            self.titulo.insert('end', prop)
            self.turma.insert('end', turma)
            self.fim_1.insert('end', str(fim))


            self.vsb.pack(side="right", fill="both")
            self.text.pack(side="left", fill="both", expand=True)
            self.text['state']='normal'
            self.text.delete(1.0, "end")
            self.text['state']='disabled'

            self.participantes=[]
            self.participantes.clear()
            self.autenticar["state"]='normal'

            for i in LISTAOBJS:
                for p in participantes:
                    print(i.numero, p)
                    if str(i.numero) == str(p):
                        var = tk.IntVar(value=1)
                    else:
                        var = tk.IntVar()
                cb = tk.Checkbutton(self.oitavoContainer, text="%s | %s %s" % (i.numero, i.nome, i.sobrenome), variable=var)
                self.text.window_create("end", window=cb)
                self.text.insert("end", "\n") # to force one checkbox per line
                self.participantes.append(var)

            self.mensagem["text"]=""
            self.mensagem["fg"]="red"

        
                
        def cadastrarReserva(self): #coleta os dados digitados e cadastra a reserva
            autor = USER.numero
            prop = self.titulo.get()
            codigoDis = random.SystemRandom().randint(100000, 999999)
            turma = self.turma.get()
            numero=[]
            for sala in range( len(self.salas)):
                if self.salas[sala].get() != 0:
                    numero.append(sala+1)

            horarios=["8:00", "10:00", "14:00", "16:00", "19:00", "20:40"]
            horario=[]
            for hor in range(len(horarios)):
                if self.horario[hor].get() != 0:
                    horario.append(horarios[hor])
            
            recorrencias=["seg", "ter", "qua", "qui", "sex", "sab"]
            recorrencia=[]
            for rec in range(len(recorrencias)):
                if self.recorr[rec].get() != 0:
                    recorrencia.append(recorrencias[rec])
            
            fim= self.fim_1.get()


            participantes=[]
            for part in range(len(LISTAOBJS)):
                try:
                    if self.participantes[part].get() != 0:
                        participantes.append(LISTAOBJS[part].numero)
                        print(participantes)

                except: pass

            obj = Reserva(autor, prop, codigoDis, numero, horario)
            if len(turma)>0: obj.turma = turma
            if len(recorrencia)>0: obj.recorrencia = recorrencia
            if len(fim)>5: 
                fim = datetime.datetime.strptime(fim, "%d/%m/%Y")
                obj.fim=fim
                print(obj.fim)
            if len(participantes)>0: 
                obj.participantes = participantes
                for participante in participantes:
                    usr = RetornaObjeto(participante)
                    usr.ident.append(codigoDis)

            print(obj, obj.turma, obj.recorrencia, obj.fim, obj.participantes)
            self.mensagem["fg"]="green"
            self.mensagem["text"]="Reserva numero %s Cadastrada com Sucesso!"%obj.codigoDis
            LISTARES.append(obj)
            self.autenticar["state"]='disabled'






        
if __name__ == "__main__":

    file1 = 'People_data.pkl'
    file2 = 'Reservas_data.pkl'
    try:        
        with open(file1, 'rb') as input2:
                builtins.LISTAOBJS = pickle.load(input2)
    except:
        with open(file1, 'wb') as input2:
                builtins.LISTAOBJS = []
    try:
        with open(file2, 'rb') as input2:
                builtins.LISTARES = pickle.load(input2)
    except:
        with open(file2, 'wb') as input2:
                builtins.LISTARES = []

    print('#'*20)
    for obj in LISTARES:
        print(obj.prop, obj.participantes)
    print('#'*20)
    for reserva in LISTARES:
        hoje = datetime.datetime.now()
        try:
            if hoje>reserva.fim:
                for aluno in reserva.participantes:
                    aluno = RetornaObjeto(aluno)
                    for res in aluno.ident:
                        if res ==reserva.codigoDis:
                            aluno.ident.remove(res)
                LISTARES.remove(reserva)

        except:pass
    print('#'*20)
    for obj in LISTARES:
        print(obj)
    print('#'*20)

    #USER=LISTAOBJS[0]
    app = LINF()
    app.mainloop()
    print ("lista")
    

    print("pessoas")
    for obj in LISTAOBJS:
        print(obj.nome, obj.ident)


    with open(file1, 'wb') as output:
        pickle.dump(LISTAOBJS, output)

    with open(file2, 'wb') as output:
        pickle.dump(LISTARES, output)
