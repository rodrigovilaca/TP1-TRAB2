class Reserva(object):
	def __init__(self, autor, prop, codigoDis,  numero, horario=[]):
		self.autor = autor
		self.prop = prop
		self.codigoDis = codigoDis
		self.turma = None
		self.numero = numero #numero das salas
		self.horario = horario
		self.recorrencia = []
		self.inicio = None
		self.fim = None
		self.participantes = []

	def __str__(self):
		try: horario = " e ".join(self.horario)
		except: horario = self.horario[0]
		try: recorrencia = "/".join(self.recorrencia)
		except: recorrencia = self.recorrencia[0]
		try: numero = "/".join(self.numero)
		except: numero = self.numero[0]
		return("%s %s | %s as %s, Sala %s"%(self.codigoDis, self.prop, recorrencia,horario, numero ))