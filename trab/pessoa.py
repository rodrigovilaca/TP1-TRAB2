import cv2, os
import numpy as np
from PIL import Image

class Pessoa(object):
	def __init__(self, nome, sobrenome, numero, prof, serv):
		self.nome = nome
		self.sobrenome = sobrenome
		self.numero = numero
		self.ident = []
		self.prof = prof
		self.servidor = serv

	def tiraFoto(self):
		def atualizaUsuarios():
			path="fotos"
			imagePaths=[os.path.join(path,f) for f in os.listdir(path)] #pega todos os endereços das imagens
			faceSamples=[] #inicia uma lista de faces
			Ids=[] #inicia uma lista de Ids
			for imagePath in imagePaths: #passa por cada imagem atrelando a face ao id contido no nome do arquivo
				pilImage=Image.open(imagePath).convert('L') #importa a imagem
				imageNp=np.array(pilImage,'uint8') #converte a imagem em array
				Id=int(os.path.split(imagePath)[-1].split(".")[1]) #extrai o id do nome
				#faces=detector.detectMultiScale(imageNp)#verifica se foi detectada face na imagem
				#for (x,y,w,h) in faces: #adiciona a imagem e o id dela às listas criadas
				faceSamples.append(imageNp)
				Ids.append(Id)

			recognizer.train(faceSamples, np.array(Ids)) #chama o método treinador do opencv
			recognizer.save('libs/users.yml') #salva os usuários atrelados ao id no arquivo "users"

		cam = cv2.VideoCapture(0) #inicia a webcam
		detector=cv2.CascadeClassifier('libs/haarcascade_frontalface_default.xml') #carrega o xml detector de face
		recognizer = cv2.face.createLBPHFaceRecognizer()
		contador=0

		while(True):
			ret, img = cam.read() #captura um frame da camera
			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #transforma em escala de cinza
			faces = detector.detectMultiScale(gray, 1.2, 7, minSize=(180,180)) #detecta uma face para tirar uma foto
			for (x,y,w,h) in faces: #desenha um retangulo em volta da face detectada
				x+=40;y+=40;w-=80;h-=70;
				cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
				contador+=1 #incrementa o contador de fotos salvas
				cv2.imwrite("fotos/User."+ self.numero +'.'+ str(contador) + ".jpg", gray[y:y+h,x:x+w]) #salva a foto na pasta "fotos" identificada pelo cpf ou matricula
				cv2.imshow('frame',img) #mostra a imagem tratada
			if cv2.waitKey(100) & 0xFF == ord('q'): #para caso a tecla "q" seja apertada
				break
			elif contador>30:#para caso tenha tirado 30 fotos
				break
		cam.release()
		cv2.destroyAllWindows()

		atualizaUsuarios()
