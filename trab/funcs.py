import numpy as np
import cv2
import os
from pessoa import Pessoa
from reserva import Reserva
import pickle
import builtins

def BuscaObjeto(Id):
	for obj in LISTAOBJS:
		if str(obj.numero) == str(Id):
			return True
	return False

def RetornaObjeto(Id):
	for obj in LISTAOBJS:
		if str(obj.numero) == str(Id):
			return obj
	return None

def RetornaReserva(Id):
	for obj in LISTARES:
		if str(obj.codigoDis) == str(Id):
			return obj
	return None


def Logar(numero):
	recognizer = cv2.face.createLBPHFaceRecognizer()
	recognizer.load('libs/users.yml')
	faceCascade = cv2.CascadeClassifier("libs/haarcascade_frontalface_default.xml");
	cam = cv2.VideoCapture(0)

	sucesso=False
	while True:
		ret, im =cam.read() #captura um frame da camera
		gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY) #transforma em escala de cinza
		faces=faceCascade.detectMultiScale(gray, 1.05,5, minSize=(180,180)) #detecta uma face no frame
		for(x,y,w,h) in faces:
			x+=40;y+=40;w-=80;h-=70;
			cv2.rectangle(im,(x,y),(x+w,y+h),(225,0,0),2) #desenha um retangulo em volta da face
			#conf = recognizer.predict(gray[y:y+h,x:x+w]) #chama o metodo que detecta faces cadastradas e armazena o id e grau de confiança
			try:
				collector = cv2.face.StandardCollector_create() 
				recognizer.predict_collect(gray[y:y+h,x:x+w], collector)
				conf = collector.getMinDist()
				pred = collector.getMinLabel()
				print(conf,pred)
				if conf<55:
					if(str(pred) == str(numero)):
						sucesso=True
			except:
				conf = recognizer.predict(gray[y:y+h,x:x+w]) #chama o metodo que detecta faces cadastradas e armazena o id e grau de confiança
			
				if str(conf) == numero:
					sucesso=True

		cv2.imshow('im',im) 
		if(sucesso):
			global LOGIN 
			LOGIN = True
			global USER 
			USER = RetornaObjeto(numero)
			print(USER)
			break
		if cv2.waitKey(10) & 0xFF==ord('q'):
			break
	cam.release()
	cv2.destroyAllWindows()
	return LOGIN, USER


