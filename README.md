Grupo: 

Rodrigo Doria Vilaça - 140031111

Arthur Ribeiro - 160113121

Universidade de Brasilia

Disciplina: Técnicas de progrmação 1.

-----------------------------------------------------------------------------

# INTRODUÇÃO

Este é o segundo trabalho da disciplina técnicas de programação 1. Nele, faremos um sistema de reservas com reconhecimento facial para os usuários do LINF da UnB na linguagem de programação python.
Ao fazer seu cadastro no programa, iremos tirar fotos com a webcam que serão usadas para o reconhecimento facial posteriormente. Para isso, usaremos a biblioteca OpenCV que nos proporciona funções de reconhecimento facial. Também serão usadas outras bibliotecas no decorrer do trabalho, que estão contidas na pasta "libs". O programa possui uma interface gráfica que foi implementada utilizando a biblioteca tkinter nativa do python. Para o armazenamento dos dados na memória, estamos usando outra biblioteca nativa do python chamada Pickle. Com ela, armazenamos em dois arquivos diferentes uma lista com todos os objetos da classe Pessoa e da classe Reserva.

## Funcionamento do programa:

Para se cadastrar, após iniciar o programa clique em "Cadastro". Então você precisará informar seu Nome, Sobrenome, matricula (caso seja aluno ou professor) ou CPF (outros usuarios), e escolher, entre as reservas já existentes, as que você fará parte caso tenha alguma. Então, ao clicar em "Cadastrar rosto", seu rosto será reconhecido e colocado em nossa base de dados, realizando seu cadastro.

Sendo um usuário cadastrado, ao clicar em Login, você informará sua matrícula e ao clicar em "Logar", seu rosto será comparado com nossa base de dados e reconhecido, realizando seu login com sucesso.

Estando logado, você terá acesso às reservas do LINF, e também poderá criar sua própria reserva.

Para criar uma nova reserva, basta clicar em "Criar reserva" no menu principal, estando logado. Lá, você informará o título da reserva, que será o propósito dela, como por exemplo "aula", ou "limpeza", ou "suporte". Caso sua reserva seja para uma aula, também será necessário indicar o código da disciplina e a turma. Também serão pedidos as salas que deseja reservar, o horário, a recorrência da reserva (caso exista) e a data de términio da reserva. Então ao clicar em "Cadastrar Reserva", ela será criada.

Para que você faça parte de uma reserva, será necessário adioná-la ao seu perfil, caso ainda não o tenha feito no cadastro. Para isso, basta clicar em "Reservas" no menu principal para vizualizar todas as reservas existentes e escolher quais você quer participar ou sair.

-----------------------------------------------------------------------------

# OBJETIVO

O objetivo deste trabalho é desenvolver nossas técnicas de programação orientada em objetos, desta vez em python. Também aprendenderemos o funcionamento da biblioteca OpenCV que será muito importante para qualquer trabalho orientado a objetos que exija algo de computação gráfica no futuro. Além disso estaremos criando um programa promissor que pode vir a ser útil e implementável no nosso ambiente de laboratórios da universidade.

-----------------------------------------------------------------------------

# ARQUITETURA

Nosso programa é dividido nos seguintes arquivos:

-> ReservasLinf.py - Onde todos detalhes da interface gráfica são implementados (utilizando a biblioteca tkinter), assim como a main do programa.

-> pessoa.py - Arquivo contendo a declaração da classe Pessoa, com todos seus atributos e métodos.

-> reserva.py - Arquivo contendo a declaração da classe Reserva, com todos seus atributos e métodos.

-> funcs.py - Arquivo contendo todas as funções que criamos para o funcionamento do programa, como Cadastrar(), Logar(), BuscaObjeto(), entre outras.

-> People_data.pkl - Arquivo que armazena todos os objetos criados da classe Pessoa na memória. Para isso, usamos a biblioteca Pickle.

-> Reservas_data.pkl - Similar ao arquivo acima, este armazena todos os objetos criados da classe Reserva na memória.

-> Pasta "fotos" - Onde armazenamos as fotos dos usuários tiradas na hora do cadastro.

-> Pasta "libs" - Contém outras bibliotecas que utilizamos no decorrer do trabalho.

-----------------------------------------------------------------------------

# CLASSES E MÉTODOS

## Classe Pessoa

Possui todos atributos necessários de uma pessoa (usuário): nome, sobrenome, numero (que pode ser tanto a matricula no caso de alunos e professores, como CPF no caso de outros usuarios) e a lista ident, que armazena as reservas da pessoa.

Também possui o método tiraFoto(), responsável por utilizar a biblioteca openCV para tirar fotos do rosto do usuário, e armazenar na pasta "fotos".

## Classe Reserva

Possui todos atributos necessários de uma reserva: autor, propósito, código da disciplina, turma, número da sala, horario, recorrencia, data de fim e o vetor de participantes da reserva.

## Classes da interface gráfica

A Interface Gráfica do programa foi construida com a biblioteca **Tkinter**, padrão da instalação do Python 3. Veja mais em https://docs.python.org/2/library/tkinter.html.
O programa também possui as classes, LINF, StartPage, Login, Cadastro e Reservas no programa principal, onde são implementados todos detalhes da interface gráfica destas "páginas" do programa, chamados de Frames.

* **LINF** - É o frame principal do programa, onde todos os outros frames são renderizados e organizados.
* **StartPage** - Frame que contém apenas uma tela de menu com as opções "Login", "Cadastro" e "Reserva", que são links para os outros Frames do programa.
* **Login** - Frame onde é possível fazer login. É digitado o número identificador e executado o reconhecimento facial com o Opencv.
* **Cadastro** - Frame que contém o formulário de cadastro de usuário. Ao final, é chamado o método da classe Pessoa que tira fotos do usuário e salva seu perfil facial.
* **Reserva** - Frame que contém o formulário de criação de reserva, acessível apenas a usuários autenticados como Professor ou Servidor. 

Cada um desses frames possui mesmo tamanho, o que possibilita que seja exibido um de cada vez, de forma que ao trocar de página, o frame selecionado é sobreposto, ficando em primeiro plano. 
Apesar de seus métodos particulares, todos os frames possuem um método de atualização de valores, que é chamado imediatamente antes da troca de página, para que a página seja exibida com valores atualizados (mensagens adequadas, campos ativos ou não, banco de dados atualizado...)

-----------------------------------------------------------------------------

# COMPILAÇÃO

Para compilar o programa é necessário ter a linguagem Python 3 instalada. O programa trabalha com as bibliotecas extras **OpenCV**, **Pillow** e **NumPy**. A instalação das bibliotecas pode ser feita executando o arquivo **"[instalador_windows.bat](instaladores/instalador_windows.bat)"(para windows)** ou **"[instalador_linux.sh](instaladores/instalador_linux.sh)"(para Linux)**. Caso possua problemas na instalação, recomendamos o uso do **instalador Anaconda** que irá facilitar o processo.

Para instalar manualmente, abra o terminal e execute os seguintes comandos (é necessário que o python esteja instalado para que os comandos `pip` funcionem):
* `pip install opencv-contrib-python` 
  * (caso você já tenha alguma versão do opencv instalada, execute antes o comando `pip uninstall opencv-python`)
* `pip install Pillow`
* `pip install numpy`

Com tudo instalado, é necessário que todos os arquivos estajam na mesma pasta do seu computador, e então basta entrar nesta pasta pelo terminal e digitar o comando:

* `python ReservasLinf.py`

-----------------------------------------------------------------------------

# Link para documentação do Doxyen

[index.html](trab/html/index.html)

-----------------------------------------------------------------------------

# DIAGRAMA DE SEQUÊNCIA

![Diagrama de Sequencia](SequenceDiagram.jpeg)

-----------------------------------------------------------------------------

# SCREENSHOTS

## Tela Inicial

![Tela Inicial](ScreenShot1.jpeg)

## Tela de Cadastro de Usuario

![Tela Cadastro](ScreenShot2.jpeg)

## Tela de Login

![Tela Login](ScreenShot3.jpeg)

## Tela de Cadastro de Reservas

![Tela Reserva](ScreenShot4.jpeg)

## Tela Inicial logado como professor / servidor

![Tela Inicial Logada](ScreenShot5.jpeg)
-----------------------------------------------------------------------------